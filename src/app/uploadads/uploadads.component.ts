import { Component, OnInit } from '@angular/core';
import {AdsService} from '../Services/ads.service';

@Component({
  selector: 'app-uploadads',
  templateUrl: './uploadads.component.html',
  styleUrls: ['./uploadads.component.css']
})
export class UploadadsComponent implements OnInit {
  postDetails: any = {};
  uid: any;
  email:any;
  sendsucces = false;

  isAleart=true;

  constructor(private adsservice: AdsService) { }
profile: any;
  ngOnInit() {
    this.profile = JSON.parse(localStorage.getItem('currentUser'));
    console.log(this.profile.user.uid);
    this.uid = this.profile.user.uid;
    this.email= this.profile.user.email;
  }


onsubmit(value) {

    this.adsservice.UserUploadAds(this.postDetails, this.uid,this.email).then(data => {
this.sendsucces = true;
    }, error => {
console.log(error);
  });
}

  closeAleart(){
    this.isAleart=false;
    window.location.reload();
  }
}
