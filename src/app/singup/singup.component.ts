 import { Component, OnInit } from '@angular/core';
// import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
// import { AngularFireAuth } from 'angularfire2/auth';
// import { Observable } from 'rxjs/Observable';
// import * as firebase from 'firebase/app';
 import {SignUpInfo} from '../AuthService/signup-info';
 import {AuthService} from '../AuthService/auth.service';
 import {Router} from '@angular/router';
 @Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {
  hide = true;
  form: any = {};
  signupInfo: SignUpInfo;
  isSignedUp = false;
  isSignUpFailed = false;
  errorMessage = '';
  successMessage: string;
  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit() {
  }
onSubmit() {

    this.authService.doRegister(this.form)
      .then(res => {
        console.log('Your account has been created')
        this.isSignedUp = true;
        this.errorMessage = '';
        this.successMessage = 'Your account has been created';
        document.getElementById('singupup').style.display = 'none';

      }, err => {
        console.log(err);
        this.isSignedUp = false;
        this.errorMessage = err.message;

      });
    // console.log(this.form);

}

}
